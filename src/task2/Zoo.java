package task2;

import java.util.ArrayList;

public class Zoo {

    public static void main(String[] args) {
        ArrayList<String> animals = new ArrayList<>();
        animals.add(0, "lion");
        animals.add(1, "monkey");
        animals.add(2, "penguin");
        animals.add(3, "alligator");
        animals.add(4, "hyena");
        animals.add(5, "lizard");
        animals.add(6, "kangaroo");
        animals.add(7, "honey badger");

        System.out.println("my lovely zoo: " + animals);
    }
}