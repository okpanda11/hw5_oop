package task4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numList = new ArrayList<>();

        Random rand = new Random();
        for (int i = 0; i < 7; i++) {
            numList.add(rand.nextInt(10));
        }
        System.out.println(numList);

        // creating iterator for numList
        Iterator<Integer> iter = numList.iterator();

        // iterating numList & adding+1, using loop
        while (iter.hasNext()) {
            System.out.print(iter.next() + 1 + "  ");
        }
    }
}