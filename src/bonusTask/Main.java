package bonusTask;

import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> teachers = new ArrayList<>();
        teachers.add("Janet Jackson");
        teachers.add("Marilyn Monroe");
        teachers.add("Nicole Kidman");
        teachers.add("Vanessa Paradis");
        teachers.add("Ben Foster");
        teachers.add("Jack Nicholson");
        teachers.add("Jesse James");
        teachers.add("Benicio del Toro");

        // 1st way to generate 1st random element
        String bestTeacher = teachers.get((int) (Math.random() * (teachers.size())));
        System.out.println("best teacher: " + bestTeacher);

        // removing 1st found random element from ArrayList
        teachers.remove(bestTeacher);

        // 2nd way to generate 2nd random element(excluded 1st)
        Random rand = new Random();
        System.out.println("worst teacher: " + teachers.get(rand.nextInt(teachers.size())));
    }
}